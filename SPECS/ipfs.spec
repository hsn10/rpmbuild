%define name go-ipfs

Summary: Inter Planetary File System
Name: %{name}
Version: 0.5.1
Release: 1%{?dist}
Group: System Environment/Daemons
License: MIT/Apache2
URL: https://ipfs.io
Source0: %{name}_v%{version}_linux-amd64.tar.gz
Source1: ipfs.service
BuildRequires: coreutils

%description
Peer to peer hypermedia protocol

%prep
%setup -q -n %{name}

%build

%install
mkdir -p %{buildroot}%{_bindir}
cp -a ipfs %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_sysconfdir}/systemd/system
cp -a %{SOURCE1} %{buildroot}%{_sysconfdir}/systemd/system

%files
%defattr(-, root, root)
%{_bindir}/ipfs
%{_sysconfdir}/systemd/system/ipfs.service

%post
systemctl daemon-reload

%preun
if [ $1 == 0 ]; then #uninstall
  systemctl unmask ipfs.service
  systemctl stop ipfs.service
  systemctl disable ipfs.service
fi

%postun
if [ $1 == 0 ]; then #uninstall
  systemctl daemon-reload
  systemctl reset-failed
fi

%changelog
* Sat May 09 2020 Radim Kolar <hsn@sendmail.cz>
- Initial packaging
