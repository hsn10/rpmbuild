How to build RPM packages
-------------------------

Install "rpm-build" and "redhat-rpm-config"  packages first. See
https://wiki.centos.org/HowTos/SetupRpmBuildEnvironment

Building package from spec file:

  1. MOCK WAY: rpmbuild -bs spec; mock ../SRPMS/foo.srpm

  2. RPM WAY:  rpmbuild -ba food.spec

Other rpm commands:

  1. Getting spec file from an existing srpm package:
     rpm -ivh extract srpm
  2. List contents of rpm file
     rpm -qlvp

